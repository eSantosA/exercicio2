object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 253
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 48
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Label2: TLabel
    Left = 40
    Top = 96
    Width = 50
    Height = 13
    Caption = 'Treinador:'
  end
  object Label3: TLabel
    Left = 40
    Top = 144
    Width = 27
    Height = 13
    Caption = 'N'#237'vel:'
  end
  object DBEdit1: TDBEdit
    Left = 120
    Top = 48
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule3.DataSource2
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 120
    Top = 141
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule3.DataSource2
    TabOrder = 1
  end
  object btn_salvar: TButton
    Left = 166
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 2
    OnClick = btn_salvarClick
  end
  object btn_cancelar: TButton
    Left = 40
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Cacelar'
    TabOrder = 3
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 120
    Top = 96
    Width = 121
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule3.DataSource2
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule3.DataSource1
    TabOrder = 4
  end
end
