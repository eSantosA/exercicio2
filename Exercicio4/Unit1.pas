unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit3, Data.DB, Vcl.Grids, Vcl.DBGrids,Unit2,
  Vcl.DBCtrls, Vcl.StdCtrls ;

type
  TForm1 = class(TForm)
    btn_inserir: TButton;
    btn_editar: TButton;
    btn_deletar: TButton;
    DBLookupListBox1: TDBLookupListBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    procedure btn_inserirClick(Sender: TObject);
    procedure btn_editarClick(Sender: TObject);
    procedure btn_deletarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn_deletarClick(Sender: TObject);
begin
  DataModule3.FDQueryPokemon.Delete();
end;

procedure TForm1.btn_editarClick(Sender: TObject);
begin
  Form2.ShowModal;
end;

procedure TForm1.btn_inserirClick(Sender: TObject);
begin
  DataModule3.FDQueryPokemon.Append();
  Form2.ShowModal;
end;

end.
